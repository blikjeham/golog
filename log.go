package main

import (
	"time"
)

type LogEntry struct {
	Datetime  time.Time
	Callsign  string
	Frequency string
	Mode      string
	RstRX     string
	RstTX     string
}

// Create a new LogEntry
func newLogEntry(callsign, frequency, mode, rstRX, rstTX string) *LogEntry {
	entry := &LogEntry{
		Callsign:  callsign,
		Frequency: frequency,
		Mode:      mode,
		RstRX:     rstRX,
		RstTX:     rstTX,
	}
	entry.Datetime = time.Now().UTC()
	return entry
}

// Store LogEntry and add it to the table
func (entry *LogEntry) Store() {
	logChannel <- entry
	storeLogEntry(entry)
}

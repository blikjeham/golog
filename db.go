package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"time"
)

// Open a connection to the database
func openDB() {
	db, err := sql.Open("sqlite3", "golog.db")
	if err != nil {
		panic(err)
	}
	app.db = db
	createLogTable()
}

// Create the LogEntry table if it does not yet exist
func createLogTable() {
	query := `
create table if not exists log (
    datetime text,
    callsign text,
    rst_rx text,
    rst_tx text,
    frequency text,
    mode text
);`
	_, err := app.db.Exec(query)
	if err != nil {
		panic(err)
	}
}

// Store the LogEntry in the database
func storeLogEntry(entry *LogEntry) {
	tx, err := app.db.Begin()
	if err != nil {
		panic(err)
	}
	query, err := tx.Prepare("insert into log (datetime, callsign, rst_rx, rst_tx, frequency, mode) values (?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}
	datetime := entry.Datetime.Format(timeFmt)
	_, err = query.Exec(datetime, entry.Callsign, entry.RstRX, entry.RstTX, entry.Frequency, entry.Mode)
	if err != nil {
		panic(err)
	}
	err = tx.Commit()
	if err != nil {
		panic(err)
	}
	if err := query.Close(); err != nil {
		panic(err)
	}
}

// Read LogEntries from the database
func readLogEntries() {
	rows, err := app.db.Query("select datetime, callsign, rst_rx, rst_tx, frequency, mode from log")
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		var datetime string
		entry := LogEntry{}
		err = rows.Scan(&datetime, &entry.Callsign, &entry.RstRX, &entry.RstTX, &entry.Frequency, &entry.Mode)
		if err != nil {
			panic(err)
		}
		entry.Datetime, err = time.Parse(timeFmt, datetime)
		if err != nil {
			panic(err)
		}
		logChannel <- &entry
	}
	if err := rows.Close(); err != nil {
		panic(err)
	}
}

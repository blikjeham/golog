package main

import (
	"database/sql"
)

type buklog struct {
	ui        userInterface
	frequency string
	mode      string
	db        *sql.DB
}

var app buklog
var logChannel = make(chan *LogEntry)

func main() {
	openDB()
	app.frequency = "14.250"
	app.mode = "SSB"
	uiInit()
	readLogEntries()
	uiRun()
	if err := app.db.Close(); err != nil {
		panic(err)
	}
}

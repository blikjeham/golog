
all: golog

golog:
	go build -o golog .

check:
	go vet .
	errcheck --blank --asserts .
	staticcheck .

.PHONY: all check

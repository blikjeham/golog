

# GoLog

GoLog is a HAM radio QSO logger written in Go. It started as a fun project to flex my Go-skills.


## Interface


### Main screen

The main screen is where you end up when the application starts. The screen is devided into the following sections.

-   Header: here you can see the current frequency and mode, and the time (in UTC)
-   The log form where you can enter new QSOs.
-   The log table where previous QSOs are shown
-   Footer with some helpful keyboard shortcuts


### Frequency window

When you press F2, you are presented with a new form where you can enter the current frequency and mode. This is than shown again in the Header of the main screen.
The way the program is set up is that you set the frequency and mode once, and then log your QSOs on that frequency and mode. When you change frequency on your radio, you must also change frequency in GoLog.


### Log table

The Log table is part of the main screen. You can switch to it using the F3 key. Now you can select log entries, view them, edit them, delete them.


## Storage

QSOs are stored in sqlite3. Each entry is stored in the log table.
During a contest you will probably have to enter more information than just the RS(T). This extra information is stored in a separate table (contest<sub>log</sub>), and links back to the log table.


## Running the program


### SQlite3

This program depends on SQlite3 for go. The first time you want to run it from source you have to do the following steps:

    go get github.com/mattn/go-sqlite3
    go install github.com/mattn/go-sqlite3

The last step (go install) requires gcc. After you have done this you can run the program normally

